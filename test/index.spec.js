const {cond, and, or, where, select, field_order, order_by, merge} = require('../index.js');
const test = require('ava');

function createConditionFrom(name, relation, value) {
  return {[name]: {relation, value}};
}

test('cond', t => {
  const res = cond('id', '>=', '100');
  const exp = createConditionFrom('id', '>=', '100');
  t.deepEqual(res, exp);
})

test('and', t => {
  const cond_1 = createConditionFrom('age',  '>=', '20');
  const cond_2 = createConditionFrom('date',  '>=', '2018-01-01');
  const cond_3 = createConditionFrom('date', '<=', '2019-01-01');
  
  const res = and(cond_1, cond_2, cond_3);
  const exp = {and: [cond_1, cond_2, cond_3]}
  
  t.deepEqual(res, exp);
})

test('and with empty params', t => {
  const res = and();
  const exp = {and: []}
  
  t.deepEqual(res, exp);
})

test('and with incorrect conditions (undefined, null or empty object)', t => {
  const cond_1 = createConditionFrom('age',  '>=', '20');
  const cond_2 = undefined;
  const cond_3 = null;
  const cond_4 = {};
  const cond_5 = createConditionFrom('date', '<=', '2019-01-01');
  
  const res = and(cond_1, cond_2, cond_3, cond_4, cond_5);
  const exp = {and: [cond_1, cond_5]}
  
  t.deepEqual(res, exp);
})

test('and with complex expressions', t => {
  const cond_1 = createConditionFrom('age',  '>=', '20');
  const cond_2 = createConditionFrom('date', '>=', '2018-01-01');
  const cond_3 = createConditionFrom('date', '<=', '2019-01-01');
  const cond_4 = {and: {cond_2, cond_3}}
  
  const res = and(cond_1, cond_4);
  const exp = {and: [cond_1, cond_4]}
  
  t.deepEqual(res, exp);
})

test('or', t => {
  const cond_1 = createConditionFrom('age',  '>=', '20');
  const cond_2 = createConditionFrom('date',  '>=', '2018-01-01');
  const cond_3 = createConditionFrom('date', '<=', '2019-01-01');
  
  const res = or(cond_1, cond_2, cond_3);
  const exp = {or: [cond_1, cond_2, cond_3]}
  
  t.deepEqual(res, exp);
})

test('or with empty params', t => {
  const res = or();
  const exp = {or: []}
  
  t.deepEqual(res, exp);
})

test('or with incorrect conditions (undefined, null or empty object)', t => {
  const cond_1 = createConditionFrom('age',  '>=', '20');
  const cond_2 = undefined;
  const cond_3 = null;
  const cond_4 = {};
  const cond_5 = createConditionFrom('date', '<=', '2019-01-01');
  
  const res = or(cond_1, cond_2, cond_3, cond_4, cond_5);
  const exp = {or: [cond_1, cond_5]}
  
  t.deepEqual(res, exp);
})

test('or with complex expressoins', t => {
  const cond_1 = createConditionFrom('age',  '>=', '20');
  const cond_2 = createConditionFrom('date', '>=', '2018-01-01');
  const cond_3 = createConditionFrom('date', '<=', '2019-01-01');
  const cond_4 = {and: [cond_2, cond_3]};
  
  const res = or(cond_1, cond_4);
  const exp = {or: [cond_1, cond_4]}
  
  t.deepEqual(res, exp);
})

test('where', t => {
  const cond_1 = createConditionFrom('age',  '>=', '20');
  const res = where(cond_1);
  const exp = {where: {and: [cond_1]}};
  t.deepEqual(res, exp);
})

test('where with incorrect conditions (undefiend, null or empty object)', t => {
  t.deepEqual(where(undefined), null);
  t.deepEqual(where(null), null);
  t.deepEqual(where({}), null);
})

test('select', t => {
  const res = select('id', 'first_name', undefined, null, 'last_name');
  const exp = {select: ['id', 'first_name', 'last_name']};
  t.deepEqual(res, exp);
})

test('select all', t => {
  const exp = {select: ['*']};
  t.deepEqual(select(), exp);
  t.deepEqual(select('*'), exp);
})

test('field_order', t => {
  const res = field_order('id', 'desc');
  const exp = {id: 'desc'};
  t.deepEqual(res, exp);
})

test('order_by', t => {
  const res = order_by(undefined, {'id': 'desc'}, null, {});
  const exp = {order_by: [{'id': 'desc'}]};
  t.deepEqual(res, exp);
})

test('merge', t => {
  const s = ['*'];
  const c = createConditionFrom('id', '>=', '100');
  const w = {and: [c]};
  const ordBy = [{'id': 'desc'}];

  const res = merge(select(), where(c), order_by({'id': 'desc'}));
  const exp = {select: s, where: w, order_by: ordBy};

  t.deepEqual(res, exp);
})