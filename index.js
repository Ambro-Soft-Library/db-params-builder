const _and = "and";
const _or = "or";
const excludeTypes = [undefined, null]

function check(exp) {
  return !excludeTypes.includes(exp) && (typeof exp === 'object' && Object.keys(exp).length > 0);
}

/**
 * The function generates condition javascript object from name, relation and value.
 * @param {*} field The name of DB field.
 * @param {*} relation The relation of prediction
 * @param {*} value The value of DB field
 */
exports.cond = (field, relation, value) => {
  return { [field]: {relation, value: value }};
}

/**
 * The function generates javascript object for 'and' expression.
 * @param {*} exp_n The order of DB expressions.
 */
exports.and = (...exp_n) => {
  return { and: exp_n.filter(exp => check(exp)) };
}

/**
 * The function generates javascript object for 'or' expression.
 * @param {*} exp_n The order of DB expressions.
 */
exports.or = (...exp_n) => {
  return { or: exp_n.filter(exp => check(exp)) };
}

/**
 * The function generates javascript object for where clause. If parameter 'exp' is single condition, The function generates appopriate javascript object.
 * @param {*} exp The DB expression, that may be single condition or group of conditions
 */
exports.where = (exp) => {
  let whereClause = null;
  if (check(exp)) {
    whereClause = (_and in exp || _or in exp) ? { where: exp } : Object.assign({}, {where: this.and(exp)});
  }
  return whereClause;
}

/**
 * The function generates select statement.
 * @param {*} fields The fields name that must be selected. If it is empty, The function generates select statement for all fields.
 */
exports.select = (...fields) => {
  return { select: fields.length === 0 ? ["*"] : fields.filter(exp => !excludeTypes.includes(exp)) };
}

/**
 * The function generates javascript object for field and its order value.
 *  @param {*} field The name of DB field.
 *  @param {*} order The value of order: "desc", "asc"
 */
exports.field_order = (field, order) => {
  return {[field]: order};
}

/**
 * The function generates order_by clause from given fields order.
 * @param {*} field_orders javascript object Array of fields order.
 */
exports.order_by = (...field_orders) => {
  return { order_by: field_orders.filter(field_order => check(field_order)) };
}

/**
 * The function generates full javascript object for DB options: select, where, order_by etc.
 * @param {*} options javascript object Array of DB statements or clauses.
 * Usage: merge(select('rec_id', 'email'), where(cond('status', '=', 2)), order_by(field_order('email', 'desc')))
 */
exports.merge = (...options) => {
  let result = {};
  options.filter(op => check(op)).forEach(opt => Object.assign(result, opt));
  return result;
}
