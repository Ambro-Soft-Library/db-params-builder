# db-params-builder

მოდული აგენერირებს general_select-ის json პარამეტრს.

გამოყენება:

თუ გვინდა *some_table*-დან გარკვეული ველების მნიშვნელობების წამოღება *lang* ენაზე, ვიქცევით შემდეგნაირად:

```javascript
import { select } from 'db-params-builder'

const params = [some_table, lang, select('id', 'first_name', 'last_name')]
axios.get("[some domain or ip]/call/general_select", {params})

```

ყველა ველის მნიშვნელობების წამოსაღებად select შეიძლება გამოვიძახოთ ასე: select() ან select('*')

თუ გვინდა *some_table*-ის მონაცემების დაფილტვრა რაიმე ველის მნიშვნელობით (ვთქვათ date), ვიქცევით შემდეგნაირად:

```javascript
import { where, and, cond } from 'db-params-builder'

const where = where(and(cond('date', '>=', '2018-01-01'), cond('date', '<=', '2019-01-01')));
const params = [some_table, lang, where]
axios.get("[some domain or ip]/call/general_select", {})

```

თუ გვინდა select-ისა და where-ს გაერთიანება, ხოლო მიღებული სიის დალაგება კლებადობით ან ზრადობით რომელიმე ველის მნიშვნელობის მიხედვით:

```javascript
import { merge, where, and, cond, order_by, field_order } from 'db-params-builder'

const select = select('first_name', 'last_name');
const where  = where(and(cond('date', '>=', '2018-01-01'), cond('date', '<=', '2019-01-01')));
const orderBy = order_by(field_order('first_name', 'asc'), field_order('last_name', 'desc'));
const params = [some_table, lang, merge(select, where, orderBy)];
axios.get("[some domain or ip]/call/general_select", {params})

```