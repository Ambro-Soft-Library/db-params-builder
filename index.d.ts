export function cond(name: string, relation: string, value: any): object

export function and(...exp_n: object[]): object

export function or(...exp_n: object[]): object

export function where(exp: object, operation: string): object

export function select(...fields: string[]): object

export function field_order(field: string, order: string): object

export function order_by(...field_orders: object[]): object

export function merge(...options: object[]): object